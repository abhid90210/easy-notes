GDrive = function() {

    var __token = false;
    var __userName = false;
    var __fileName = "MyFile.json";
    var __fileID = null;
    var authUnsuccessful = new Event(this);
    var authSuccessful = new Event(this);
    var dataLoaded = new Event(this);
    var userProfileLoaded = new Event(this);
    var fileDeleted = new Event(this);

    // Run this EACH time on browserAction to fetch token, i.e. when user click the button
    chrome.runtime.sendMessage({message: "getToken"}, function(response) {
        if(response.token) {
            console.log("GDrive :: bootstrapAuthTokenSuccess");
            console.log(response);
            __token = response.token;
            __userName = response.userName;
            authSuccessful.notify(response);
        }
        else {
            //console.log("Token fetch unsuccessfull");
            authUnsuccessful.notify();
        }
    });

    // Display auth permission page (only first time) and fetch Auth token from Google
    var doSignIn = function() {
        chrome.identity.getAuthToken( {'interactive': true }, function(token) {
            // :(( Auth unsuccessful
            if(chrome.runtime.lastError) {
                console.log(chrome.runtime.lastError);
                chrome.runtime.sendMessage({message: "tokenSetFailed"},
                    function(response) {
                        //console.log("GDRIVE token Unsuccessfull");
                        authUnsuccessful.notify();
                }); //chrome.runtime
            } else 
            {
                // :)) Auth successful
                //console.log('Token Acquired:'+token+'. See www for details');
                __token = token;
                //Get user profile info
                getUserProfileInfo();
                // Send token to background.js
                chrome.runtime.sendMessage({message: "tokenSet", token: __token},
                    function(response) {
                        //console.log("GDrive");
                        //console.log(response);
                        authSuccessful.notify({userName: null});
                }); //chrome.runtime
            }
        });
    }; //doSignIn

    // Log Out button pressed, so log out by removing token
    var doSignOut = function() {
            chrome.identity.removeCachedAuthToken({token: __token}, function() {
                // local cached token reomved, notify listeners
                authUnsuccessful.notify();
            });
            // also remove background.js's copy of token and username
            chrome.runtime.sendMessage({message: "signOut"},
                    function(response) {
                        console.log(response);
                    });
    };

    var getUserProfileInfo = function() {

                var xhr = new XMLHttpRequest();
                xhr.open('GET', 'https://www.googleapis.com/oauth2/v1/userinfo');
                xhr.responseType = "json";
                xhr.setRequestHeader('Authorization', 'Bearer ' + __token);
                xhr.onload = function(e) {
                    //console.log(xhr.response);
                    // cache the user name also
                    __userName = xhr.response.name;
                    chrome.runtime.sendMessage({message: "userNameFetched", userName: __userName},
                    function(response) {
                        //console.log(response);
                        userProfileLoaded.notify(__userName);
                }); //chrome.runtime

                    };
                xhr.send();
    };

    // Private function
    var checkIfFileExistsREADorCREATE = function() {
                var xhr = new XMLHttpRequest();
                var queryString = encodeURI("maxResults=1&q=title='" + __fileName + "'&fields=items(id,title, exportLinks, webContentLink, downloadUrl)");
                xhr.open('GET', 'https://www.googleapis.com/drive/v2/files?' + queryString);
                xhr.responseType = "json";
                xhr.setRequestHeader('Authorization', 'Bearer ' + __token);
                xhr.onload = function(e) {
                    //console.log(xhr.response);
                    if(xhr.response.items[0]) {
                        //alert("Found the file: " + xhr.response.items[0].title);
                        loadDataFromFile(xhr.response.items[0].id,  xhr.response.items[0].downloadUrl);
                    } else {
                        alert ("Creating new file in Google Drive: " + __fileName);
                        createNewFile();
                    }
                    //either case nodify controller that data loaded, enable logout boutton etc.
                    //console.log("Auth");
                    //console.log(res);
                    //dataLoaded.notify(res);
                };
                xhr.send();
    };

    var checkIfFileExistsUPDATEorCREATE = function(data) {
                var xhr = new XMLHttpRequest();
                // Check if the file exists on GDrive
                var queryString = encodeURI("maxResults=1&q=title='" + __fileName + "'&fields=items(id,title, exportLinks, webContentLink, downloadUrl)");
                xhr.open('GET', 'https://www.googleapis.com/drive/v2/files?' + queryString);
                xhr.responseType = "json";
                xhr.setRequestHeader('Authorization', 'Bearer ' + __token);
                xhr.onload = function(e) {

                    console.log(xhr.readyState);
                    console.log(xhr.status);

                    switch(xhr.status) {
                        //case 404: alert("File Not Found"); break;
                        //case 500: alert("Server Error"); break;
                        case 401: alert("Unauthorized: Session expiered. Please log in again."); doSignOut(); break;
                        //case 0: alert("Request Aborted"); break;
                        //case 200: alert("Great Success"); break;
                        //default: alert("Unknown Error" + xhr.status);
                    }

                    if(xhr.status == 200) {
                        console.log(xhr.status);
                        if(xhr.response.items[0]) {
                            //console.log(xhr.response);
                            //alert("Found the file(UPDATE): " + xhr.response.items[0].title);
                            __fileID = xhr.response.items[0].id;
                            loadDataFromFile(xhr.response.items[0].id,  xhr.response.items[0].downloadUrl);
                        } else {
                            alert ("Creating new file in Google Drive: " + __fileName);
                            createNewFile(data);
                        }
                    }
                    //either case nodify controller that data loaded, enable logout boutton etc.
                    //console.log("Auth");
                    //console.log(res);
                    //dataLoaded.notify(res);
                };

                xhr.send();
    };


    var loadDataFromFile = function(id, url) {

        console.log("Loading Data From File");

        var xhr = new XMLHttpRequest();
        xhr.open('GET', url);
        xhr.responseType = "json";
        xhr.setRequestHeader('Authorization', 'Bearer ' + __token);
        xhr.onload = function(e) {

                console.log(xhr.status + "File Load");
                switch(xhr.status) {
                    case 404: alert("404: File Not Found"); break;
                    case 500: alert("500: Server Error"); break;
                    case 401: alert("401: Unauthorized: Session expiered. Please log in again."); doSignOut(); break;
                    case 0: alert("0: Request Aborted"); break;
                    case 200: alert("200: Great Success"); break;
                    default: alert("Unknown Error" + xhr.status);
                }

                console.log("loadDataFromFile");
                // Fetch remove list from backgroundjs
                var itemsArray = [];
                itemsArray = xhr.response.items;
                // async
                chrome.runtime.sendMessage({message: "getRemovedItemsList"}, function(res) {
                        // remove common items between it and list fetched from GDrive
                        itemsArray = xhr.response.items.filter( function(x) {
                            return res.removedItemsList.indexOf(x) < 0 
                        });
                        console.log("Sending " + itemsArray);
                        // send to controller for merging
                        dataLoaded.notify(itemsArray);
                });

                // get current item list from controller and send to GDrive
                // sync complete
            };
        xhr.send();

    };

    var createNewFile = function(data, id) {
            
                const boundary = '---------314159265358979323846'; 
                const delimiter = "\r\n--" + boundary + "\r\n"; 
                const close_delim = "\r\n--" + boundary + "--"; 

                //var text = JSON.stringify(data) || JSON.stringify({title: "This is the Content"}); 
                // Data should be already stringified
                var text = data || JSON.stringify({title: "This is the Content"}); 

                var contentType = "text/html";
                var metaData = {
                            'mimeType': contentType,
                            'title': __fileName
                            };

                var multipartRequestBody =
                    delimiter + 'Content-Type: application/json\r\n\r\n' +
                    JSON.stringify(metaData) + 
                    delimiter + 'Content-Type: ' + contentType + '\r\n' + '\r\n' + 
                    text + 
                    close_delim;

                var xhr = new XMLHttpRequest();
                if(id) {
                    console.log("Uploading " + text);
                    xhr.open('PUT', 'https://www.googleapis.com/upload/drive/v2/files/' + __fileID + '?uploadType=multipart');
                } else {
                    xhr.open('POST', 'https://www.googleapis.com/upload/drive/v2/files?uploadType=multipart');
                }
                xhr.responseType = "json";
                xhr.setRequestHeader('Content-Type', 'multipart/mixed; boundary="' + boundary +'"');
                xhr.setRequestHeader('Authorization', 'Bearer ' + __token);
                xhr.onload = function(e) {

                    __fileID = xhr.response.id;
                    //console.log("File Not Found on GDrive; Create a new one.");
                };
                xhr.send(multipartRequestBody);

    }; // createNewFile()

    var updateFile = function(data) {

        // Data should be already stringified
        var text = data || JSON.stringify({title: "This is the Content"}); 
        var xhr = new XMLHttpRequest();

        xhr.responseType = "json";
        xhr.open('PUT', 'https://www.googleapis.com/upload/drive/v2/files/' + __fileID + '?uploadType=media');
        xhr.setRequestHeader('Content-Type', 'text/html');
        xhr.setRequestHeader('Authorization', 'Bearer ' + __token);

        xhr.onload = function(e) {
            console.log("After Upload");
            console.log(xhr.response);
        };

        console.log("sending" + text);
        xhr.send(text);

    }; //updateFile();


    /*
    var deleteFile = function(fileID) {

                console.log("Deleting File");

                var fileID = fileID || __fileID;
                var xhr = new XMLHttpRequest();
                xhr.open('DELETE', 'https://www.googleapis.com/drive/v2/files/' + fileID);
                xhr.responseType = "json";
                xhr.setRequestHeader('Authorization', 'Bearer ' + __token);
                xhr.onload = function(e) {
                    console.log(xhr.status + "File Delete");
                    console.log(fileID);
                    switch(xhr.status) {
                        case 404: alert("404: File Not Found"); break;
                        case 500: alert("500: Server Error"); break;
                        case 401: alert("401: Unauthorized: Session expiered. Please log in again."); doSignOut(); break;
                        case 0: alert("0: Request Aborted"); break;
                        case 200: alert("200: Great Success"); break;
                        default: alert("Unknown Error" + xhr.status);
                    }

                    if(xhr.status == 200) {
                        if(xhr.response.items[0]) {
                            alert("Deleting File!");
                            //fileDeleted.notify();
                        } 
                    } else {
                        alert ("Not able to delete file.");
                    }
                    fileDeleted.notify();
                };
                xhr.send();
    };
    */

    // Public Functions
    var fetchLoggedInUserData = function(data) {
            if(__token) {               // save a round trip if current token is not defined, instead of being rejected by Google
                checkIfFileExistsREADorCREATE(data);
            } else {
                /* NOOP */
            }
    };

    var writeModelDataToDrive = function(data) {
            if(__token) {               // save a round trip if current token is not defined, instead of being rejected by Google
                checkIfFileExistsUPDATEorCREATE(data);
            } else {
                alert("Not Logged In");
            }
        //console.log(JSON.stringify(data));
        //deleteOldFile();
        //createNewFile(data);
        // -- OR
        //updateContentsOfTheFile(data);
    };


    var getToken = function() {
        return __token;
    };

    return {
        signIn: doSignIn,
        signOut: doSignOut,
        writeJSON: writeModelDataToDrive,
        getToken: getToken,
        fetchLoggedInUserData: fetchLoggedInUserData,
        //deleteFile: deleteFile,
        createNewFile: createNewFile,
        updateFile: updateFile,
        // Public Events
        authUnsuccessful: authUnsuccessful,
        authSuccessful: authSuccessful,
        dataLoaded: dataLoaded,
        userProfileLoaded: userProfileLoaded,
        fileDeleted: fileDeleted
    };

}(); //GDrive Singleton
