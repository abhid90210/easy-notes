var __token = false;
var __userName = null;
var __itemsRemovedList = [];

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if(request.message == "getToken") {
            console.log("bg!getToken");
            sendResponse({token: __token, userName: __userName});
        }
        if(request.message == "tokenSet") {
            __token = request.token;
            console.log("bg!tokenSet");
            sendResponse({message: "Token Saved: "+__token});
        }
        if(request.message == "userNameFetched") {
            __userName = request.userName;
            console.log("bg!userNameFetched");
            //console.log(request);
            //console.log(__userName);
            sendResponse({message: "User Name Saved: "+__userName});
        }
        if(request.message == "tokenSetFailed") {
            console.log("bg!tokenSetFailed");
            alert("Unable to connect to Google Drive.!");
            __token = false;
            __userName = null;
            sendResponse({message: "Token Not Saved: "});
        }
        if(request.message == "getUserName") {
            console.log("bg!getUserName");
            sendResponse({token: __userName});
        }
        if(request.message == "signOut") {
            console.log("bg!signOut");
            __token = false;
            __userName = null;
            sendResponse({message: "Deleted local"});
        }
        if(request.message == "removingItem") {
            console.log("bg!removingItem");
            __itemsRemovedList.push( request.item );
            sendResponse({message: "Stored deleted item in local cache"});
        }
        if(request.message == "getRemovedItemsList") {
            console.log("bg!getRemovedItemsList");
            sendResponse({removedItemsList: __itemsRemovedList});
        }

});
