'use strict';

function disableButton(button)
{
    button.prop('disabled', true);
}
function enableButton(button)
{
    button.prop('disabled', false);
}

/**
 * The Model
 */
function notesModel(items) {
    this._items = items;
    this._notesAreaText = null;
    this._selectedIndex = -1;

    this.itemAdded = new Event(this);
    this.itemRemoved = new Event(this);
    this.selectedIndexChanged = new Event(this);
    this.allItemsRemoved = new Event(this);
    this.itemsLoaded = new Event(this);
}

notesModel.prototype = {
    getItems: function() {
                  return [].concat(this._items);
              },

    addItem: function(item) {
                 this._items.push(item);
                 this.itemAdded.notify({item:item});
             },

    removeItemAt: function(index) {
                      var item;
                      item = this._items[index];
                      this._items.splice(index, 1);
                      this.itemRemoved.notify({item:item});
                      if(index == this._selectedIndex) {
                          this.setSelectedIndex(-1);
                      }
                  },

    getSelectedIndex: function() {
                          return this._selectedIndex;
                      },

    setSelectedIndex: function(index) {
                          var previousIndex;
                          previousIndex = this._selectedIndex;
                          this._selectedIndex = index;
                          this.selectedIndexChanged.notify({previous: previousIndex});
                      },

    removeAllItems: function() {
                        this._items = [];
                        this.allItemsRemoved.notify();
                        console.log('All removed');
                    },

    loadItems: function(items) {
                     this._items = items;
                     this.itemsLoaded.notify();
                     console.log('All added');
                 },
    mergeItems: function(items) {
                    //console.log(this.arrayUnique(this._items.concat(items)));
                    this.loadItems(this.arrayUnique(this._items.concat(items)));
                    console.log("All Merged");
                },

    arrayUnique: function(array) {
                    var a = array.concat();
                    for(var i=0; i<a.length; ++i) {
                        for(var j=i+1; j<a.length; ++j) {
                            if(a[i] === a[j])
                                a.splice(j--, 1);
                        }
                    }
                    return a;
                },

    getStringifyableData: function() {
                              return {items: this._items, notesAreaText: this._notesAreaText};
                          }
};

/**
 * View Class
 */
function notesView(model, elements) {
    this._model = model;
    this._elements = elements;

    this.listModified = new Event(this);
    this.addButtonClicked = new Event(this);
    this.delButtonClicked = new Event(this);
    this.syncButtonClicked = new Event(this);
    this.notesAreaKeyPress = new Event(this);
    this.logInButtonClicked = new Event(this);
    this.logOutButtonClicked = new Event(this);

    var _this = this;

    //attach model listeners
    this._model.itemAdded.attach(function() {
        _this.rebuildList();
    });
    this._model.itemRemoved.attach(function() {
        _this.rebuildList();
    });
    this._model.allItemsRemoved.attach(function() {
        _this.rebuildList();
    });
    this._model.itemsLoaded.attach(function() {
        _this.rebuildList();
    });


    //attach listeners to HTML controls
    this._elements.list.change(function(e) {
        _this.listModified.notify({index: e.target.selectedIndex });
    });
    this._elements.addButton.click(function() {
        _this.addButtonClicked.notify();
    });
    this._elements.delButton.click(function() {
        _this.delButtonClicked.notify();
    });
    this._elements.syncButton.click(function() {
        _this.syncButtonClicked.notify();
    });
    this._elements.logInButton.click(function() {
        _this.logInButtonClicked.notify();
    });
    this._elements.logOutButton.click(function() {
        _this.logOutButtonClicked.notify();
    });
    // notesArea key press event to store whatever is written in the txtbox to be stored for immidiate retrival
    this._elements.notesArea.keyup(function() {
        _this.notesAreaKeyPress.notify();
    });

    // Misc UI toggle
    this._elements.listAllNotesButton.click(function() {
        _this._elements.delButton.toggle();
        _this._elements.list.toggle();
        _this._elements.notesArea.toggle();
    });
    this._elements.list.dblclick(function() {
        _this._elements.delButton.toggle();
        _this._elements.list.toggle();
        _this._elements.notesArea.toggle();
    });
    this._elements.notesArea.dblclick(function() {
        _this._elements.delButton.toggle();
        _this._elements.list.toggle();
        _this._elements.notesArea.toggle();
    });

}

notesView.prototype = {
    show: function() {
              this.rebuildList();
          },

    rebuildList: function() {
                     var list, items, key;
                     list = this._elements.list;
                     list.html('');
                     items = this._model.getItems();
                     for(key in items) {
                         if(items.hasOwnProperty(key)) {
                             list.append($('<option>' + items[key] + '</option>'));
                         }
                     }
                     this._model.setSelectedIndex(-1);
                 },

    getNotesAreaText: function() {
                          return this._elements.notesArea.val();
                      },

    setNotesAreaText: function(text) {
                            this._elements.notesArea.val(text);
                      }
};

/**
 * The Controller
 */
function notesController(model, view) {
    this._model = model;
    this._view = view;
    var _this = this;

    //Attach listner to the veiw
    this._view.listModified.attach(function(sender, args) {
        _this.updateSelected(args.index);
    });
    this._view.addButtonClicked.attach(function() {
        _this.addItem();
    });
    this._view.delButtonClicked.attach(function() {
        _this.delItem();
    });
    this._view.syncButtonClicked.attach(function() {
//        _this.addItem();
        _this.syncData();
    });
    this._view.notesAreaKeyPress.attach(function() {
        _this.saveNotesAreaText();
    });
    this._view.logInButtonClicked.attach(function() {
        _this.startGoogleDriveAuth();
    });
    this._view.logOutButtonClicked.attach(function() {
        _this.stopGoogleDriveAuth();
    });
    //Listen to external events (GDrive's)
    GDrive.authSuccessful.attach(function(send, res) {
        _this._view._elements.logInButton.hide();
        _this._view._elements.logOutButton.show();
        if (res.userName) {
            console.log("Inside");
            _this.printUserName(res.userName);
        };
        // Dont load user notes data since this is the cached login; load user data when sync is pressed
        //_this.loadLoggedInUsersData();                      // Since we are logged in, lets load users notes from his Google Drive
        enableButton(_this._view._elements.logOutButton);
    });
    GDrive.authUnsuccessful.attach(function() {
        _this._view._elements.logInButton.show();         // Show LogIn Button
        _this._view._elements.logOutButton.hide();        // Hide LogOut Button
        enableButton(_this._view._elements.logInButton);
         _this.printUserName("Please Log In.");
    });
    GDrive.dataLoaded.attach(function(send, res) {
        _this.mergeNotes(res);
   });
    GDrive.userProfileLoaded.attach(function(send, res) {
        _this.printUserName(res);
        enableButton(_this._view._elements.logOutButton);
    });
    // File has been deleted while merging, now create a new file with current model data
    GDrive.fileDeleted.attach(function(send, res) {
        _this.createNewGDriveFile();
    });

}

notesController.prototype = {
    addItem: function() {
                 /*var item = window.prompt('Add item:', '');
                 if(item) {
                     this._model.addItem(item);
                 }*/
                 var item = this._view.getNotesAreaText();
                 if(item.trim()) {
                     this._model.addItem(item);
                     // Store local
                     chrome.storage.local.set( {'items': this._model._items}, function() {} );
                     // Clear Notes area text and clear local store notes area text also
                     this._view.setNotesAreaText("");
                     chrome.storage.local.remove( 'notesArea', function() { console.log('Removed'); });
                 }
            },

    delItem: function() {
                 var index;
                 index = this._model.getSelectedIndex();
                 if(index !== -1) {
                     // Store the deleted item in background.js removed item list to help in syncing with GDrive
                     chrome.runtime.sendMessage({ message: "removingItem", item: this._model._items[index] }, function(response) {
                     });
                     this._model.removeItemAt(this._model.getSelectedIndex());
                     // Also sync local immidiately
                     chrome.storage.local.set( {'items': this._model._items}, function() {} );                    
                 }
             },

    updateSelected: function(index) {
                        this._model.setSelectedIndex(index);
                    },

    syncData: function() {
                  disableButton(this._view._elements.syncButton);
                  /* var _this = this;
                  chrome.storage.local.set( {'items': this._model._items}, function() {});
                  chrome.storage.sync.set( {'items': this._model._items}, function() { 
                      console.log('Synced');
                      enableButton(_this._view._elements.syncButton); 
                  }); */
                  //console.log(this._model);
                  //console.log( JSON.stringify( this._model.getStringifyableData() ));
                  GDrive.writeJSON(JSON.stringify( this._model.getStringifyableData() ));
                  //GDrive.writeJSON(this._model._items);
              },

    saveNotesAreaText: function() {
                           var item = this._view.getNotesAreaText();
                           if(item.trim()) {
                               console.log(item);
                               chrome.storage.local.set( {'notesArea': item}, function() {});
                           } else {
                               chrome.storage.local.remove( 'notesArea', function() { console.log('Removed'); });
                           }
                    },

    startGoogleDriveAuth: function() {
                            disableButton(this._view._elements.logInButton);
                            this._view._elements.userNameDiv.text("Contacting Google...");
                            GDrive.signIn();
                          },

    stopGoogleDriveAuth: function() {
                            GDrive.signOut();
                          },

    loadLoggedInUsersData: function() {
                             GDrive.fetchLoggedInUserData();
                           },

    printUserName: function(userName) {
                       this._view._elements.userNameDiv.text(userName);
                   },

    mergeNotes: function(res) {

                    var itemsArray = [];
                    itemsArray = res;
                    var that = this;
                    // async
                    console.log(res);
                    chrome.runtime.sendMessage({message: "getRemovedItemsList"}, function(bgRes) {
                            // remove common items between it and list fetched from GDrive
                            itemsArray = res.filter( function(x) {
                                return bgRes.removedItemsList.indexOf(x) < 0 
                            });
                            // Merge clean list fetched from GDrive with current model
                            that._model.mergeItems(itemsArray);
                            // get new current model item list to upload to GDrive
                            console.log("Items Merged!! Now uploading ::");
                            console.log( that._model.getStringifyableData() );
                            GDrive.updateFile( JSON.stringify( that._model.getStringifyableData() ) );

                            //GDrive.writeData(JSON.stringify( this._model.getStringifyableData() ));
                            // mergeNOtes is callled only after fetching data from Gdrive, now delete the file, wait for notify, then reupload new data
                            //GDrive.deleteFile();
                            //enableButton(this._view._elements.syncButton);
                            // get current item list from controller and send to GDrive
                            // sync complete
                            
                            
                    });

                },

    createNewGDriveFile: function() {
                    // GDrive.createNewFile(JSON.stringify( this._model.getStringifyableData() ));
                    enableButton(this._view._elements.syncButton);
                     }
};

window.onload = function() {

    $('#list').hide();
    $('#minusBtn').hide();
    $('#logInBtn').hide();
    $('#userName').text("Please Log In.");


    var model = new notesModel(['PHP', 'JavaScript']);
    var view  = new notesView(model, {
                        'list': $('#list'),
                        'addButton': $('#plusBtn'),
                        'delButton': $('#minusBtn'),
                        'notesArea': $('#txtArea'),
                        'listAllNotesButton': $('#listAllNotesBtn'),
                        'syncButton': $('#syncBtn'),
                        'logInButton': $('#logInBtn'),
                        'logOutButton': $('#logOutBtn'),
                        'userNameDiv': $('#userName')
        });
    var controller = new notesController(model, view);
    view.show();

    // Init:: Load local copy of the notes
    chrome.storage.local.get('notesArea', function(items) {
        // add error/failur condition here
        //console.log(items.notesArea);
        view.setNotesAreaText(items.notesArea);
    });

    chrome.storage.local.get('items', function(items) {
        if(items[0]) {
            model.removeAllItems();
            console.log("Loading local");
            console.log(items);
            model.loadItems(items.items);
        } else {
            // No items in local cache
            console.log(items);
        }
    });

};

/**
// -------- OLD CODE
var EasyNotesLoader =(function() {

    var STATE_START = 1;
    var STATE_ACQUIRING_AUTHTOKEN = 2;
    var STATE_AUTHTOKEN_ACQUIRED = 3;
    var state = STATE_START;
    var signin_button, logout_button, save_button, txtArea, deb;
    var allNotes = [];


    //Load i18n text
    function loadI18nMessages() {
        function setProperty(selector, prop, msg) {
            document.querySelector(selector)[prop] = chrome.i18n.getMessage(msg);
        }
        setProperty('#btnSignIn', 'innerText', 'btnSignIn');
        setProperty('#btnLogOut', 'innerText', 'btnLogOut');
        setProperty('#btnSave', 'innerText', 'btnSave');
    }

    //Misc helper
    function disableButton(button)
    {
        button.setAttribute('disabled');
    }
    function enableButton(button)
    {
        button.removeAttribute('disabled');
    }
    function hideButton(button)
    {
        button.style.display = 'none';
    }
    function showButton(button)
    {
        button.style.display = 'block';
    }


    //Enable or Disable buttons according to the state of authorization
    function changeState(newState)
    {
        state = newState;
        switch(state) {
            case STATE_START:
                showButton(signin_button);
                enableButton(signin_button); 
                disableButton(logout_button);
                hideButton(logout_button);
                break;
            case STATE_ACQUIRING_AUTHTOKEN:
                console.log('Acquiring token ...');
                disableButton(signin_button);
                disableButton(logout_button);
                break;
            case STATE_AUTHTOKEN_ACQUIRED:
                disableButton(signin_button); 
                hideButton(signin_button);
                showButton(logout_button);
                enableButton(logout_button); 
                break;
        }
    }

    function doSignIn() {
        changeState(STATE_ACQUIRING_AUTHTOKEN);
        chrome.identity.getAuthToken( {'interactive': true }, function(token) {
            if(chrome.runtime.lastError) {
                console.log(chrome.runtime.lastError);
                changeState(STATE_START);
            } else {
                console.log('Token Acquired:'+token+'. See www for details');
                changeState(STATE_AUTHTOKEN_ACQUIRED);
            }
        });
    }
    
    function revokeToken() {
        chrome.identity.getAuthToken( {'interactive': false },
            function(current_token) {
                if(!chrome.runtime.lastError) {
                    chrome.identity.removeCachedAuthToken({token: current_token}, function() {});
                    var xhr = new XMLHttpRequest();
                    xhr.open('GET', 'https://accounts.google.com/o/oauth2/revoke?token=' + current_token);
                    xhr.send();
                    changeState(STATE_START);
                    console.log("removing token"+current_token);
                }
        });
    }

    function saveNote() {
        if(txtArea.value) {
            allNotes[allNotes.length+1] = txtArea.value;    
            txtArea.value = '';
            deb.innerText = "Written";
        }
        else {
            deb.innerText = "Not Written";

        }
    }

    return {
        onload: function() {
            loadI18nMessages();
            deb = document.querySelector('#debugMsg');
            txtArea = document.querySelector('#txtArea');

            signin_button = document.querySelector('#btnSignIn');
            signin_button.addEventListener('click', doSignIn);

            logout_button = document.querySelector('#btnLogOut');
            logout_button.addEventListener('click', revokeToken); 

            save_button = document.querySelector('#btnSave');
            save_button.addEventListener('click', saveNote); 

            changeState(STATE_START);
        }
    };

})();

window.onload = EasyNotesLoader.onload;
**/
